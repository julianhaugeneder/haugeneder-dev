import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Cv from '../views/Cv.vue'
import About from '../views/About.vue'
import MarkdownEdit from '../views/MarkdownEdit.vue'
import GraphEdit from '../views/GraphEdit.vue'
import ODTH from '../views/ODTH.vue'
import Atpsus from "../views/Atpsus";
import Contact from "../views/Contact";
import Upcoming from "../views/Upcoming";
import Projects from "../views/Projects";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/projects',
    name: 'Projects',
    component: Projects
  },
  {
    path: '/cv',
    name: 'Cv',
    component: Cv
  },
  {
    path: '/mdedit',
    name: 'MarkdownEdit',
    component: MarkdownEdit
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/contact',
    name: 'Contact',
    component: Contact
  },
  {
    path: '/graphedit',
    name: 'GraphEdit',
    component: GraphEdit
  },
  {
    path: '/odth',
    name: 'Odth',
    component: ODTH
  },
  {
    path: '/atpsus',
    name: 'Atpsus',
    component: Atpsus
  },
  {
    path: '/upcoming',
    name: 'Upcoming',
    component: Upcoming
  },
]

const router = new VueRouter({
  routes
})

export default router
